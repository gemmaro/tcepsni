# Tcepsni

Tcepsni (`"inspect".reverse.capitalize`) gem is a parser library for inspected strings of Ruby objects.
It doesn't work comprehensively, and *any suggestions are welcome*.

## Installation

Install the gem and add to the application's `Gemfile` by executing `bundle add tcepsni`.
If Bundler is not being used to manage dependencies, install the gem by executing `gem install tcepsni`.

## Usage

Tcepsni has a parsing method `Tcepsni.parse(str)`, which returns a parsed Ruby object.

## Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake test` to run the tests.
You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [RubyGems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome.

## License

Copyright 2023, 2024 gemmaro.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at [Apache Software Foundation - Apache License, Version 2.0][asl].

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

[asl]: http://www.apache.org/licenses/LICENSE-2.0

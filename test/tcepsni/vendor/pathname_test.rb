require 'test_helper'
require 'tcepsni/vendor/pathname'

module Tcepsni
  module Vendor
    class PathnameTest < ::TestCase
      test 'Pathname' do
        val = ::Pathname.new('/home/gemmaro/sample.rb')
        str = '#<Pathname:/home/gemmaro/sample.rb>'
        assert_equal str, val.inspect
        assert_equal val, Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::Pathname])
      end

      test 'basic' do
        str = '#<Pathname:/home/gemmaro/src/sample-app>'
        assert_equal ::Pathname.new('/home/gemmaro/src/sample-app'),
                     Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::Pathname])
      end

      test 'Rails application configuration' do
        str = '#<Rails::Application::Configuration:0x00007f97d8b729e8 @root=#<Pathname:/home/gemmaro/src/sample-app>>'
        val = Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::Pathname])
        assert_instance_of ::Tcepsni::Object, val
      end

      test '' do
        str = '#<Rails::Application::Configuration:0x00007f97d8b729e8 @root=#<Pathname:/home/gemmaro/src/sample-app>, @generators=#<Rails::Configuration::Generators:0x00007f97d8dadb68>>'
        val = Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::Pathname])
        assert_instance_of ::Tcepsni::Object, val
      end
    end
  end
end

require 'test_helper'
require 'tcepsni/vendor/string_scanner'

module Tcepsni
  module Vendor
    class StringScannerTest < ::TestCase
      test 'string scanner' do
        val = ::StringScanner.new('hello')
        val.skip('hel')
        str = '#<StringScanner 3/5 "hel" @ "lo">'
        assert_equal str, val.inspect
        parsed = Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::StringScanner])
        assert_instance_of Tcepsni::Vendor::StringScanner, parsed
        assert_equal 3, parsed.pos
        assert_equal 5, parsed.total
        assert_equal 'hel', parsed.scanned
        assert_equal 'lo', parsed.rest
      end

      test 'string scanner with quotes' do
        val = ::StringScanner.new('"Hello"')
        val.skip('"He')
        str = '#<StringScanner 3/7 "\"He" @ "llo\"">'
        assert_equal str, val.inspect
        parsed = Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::StringScanner])
        assert_instance_of Tcepsni::Vendor::StringScanner, parsed
        assert_equal 3, parsed.pos
        assert_equal 7, parsed.total
        assert_equal '"He', parsed.scanned
        assert_equal 'llo"', parsed.rest
      end

      class PseudoStringScannerTest < TestCase
        StringScanner = scanner_class = ::Class.new
        test 'pseudo string scanner' do
          val = scanner_class.new
          str = val.inspect
          pat = /#<Tcepsni::Vendor::StringScannerTest::PseudoStringScannerTest::StringScanner:0x[0-9a-f]{16}>/
          assert_match pat, str
          assert_instance_of Tcepsni::Object, Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::StringScanner])
        end
      end
    end
  end
end

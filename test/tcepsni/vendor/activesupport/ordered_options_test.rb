require 'test_helper'
require 'tcepsni/vendor/activesupport/ordered_options'

module Tcepsni
  module Vendor
    module ActiveSupport
      class OrderedOptionsTest < ::TestCase
        test 'ActiveSupport ordered options' do
          str = '#<ActiveSupport::OrderedOptions {:enabled=>true, :index_name=>"index"}>'

          val = ::ActiveSupport::OrderedOptions.new
          val.enabled = true
          val.index_name = 'index'

          assert_equal str, val.inspect
          assert_equal val, Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::ActiveSupport::OrderedOptions])
        end

        test 'ActiveSupport ordered options part 2' do
          str = '#<ActiveSupport::OrderedOptions {:enabled=>true}>'

          val = ::ActiveSupport::OrderedOptions.new
          val.enabled = true

          assert_equal str, val.inspect
          assert_equal val, Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::ActiveSupport::OrderedOptions])
        end
      end
    end
  end
end

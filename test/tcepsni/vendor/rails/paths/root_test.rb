require 'test_helper'
require 'tcepsni/vendor/rails/paths/root'

module Tcepsni
  module Vendor
    module Rails
      module Paths
        class RootTest < ::TestCase
          test 'Rails::Paths::Root' do
            assert_instance_of Tcepsni::Vendor::Rails::Paths::Root,
                               Tcepsni.parse('#<Rails::Paths::Root:0x00007fee4530e6d0 ...>',
                                             vendors: [::Tcepsni::Vendor::Rails::Paths::Root])
          end

          test 'Rails::Paths::Root part 2' do
            str = ::Rails::Paths::Root.new('somewhere').inspect
            assert_match(/#<Rails::Paths::Root:0x[0-9a-f]{16} @path="somewhere", @root={}>/, str)
            assert_instance_of Tcepsni::Vendor::Rails::Paths::Root,
                               Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::Rails::Paths::Root])
          end

          test 'Rails::Paths::Root part 3' do
            str = '#<Rails::Paths::Root:0x00007f97d8b264d0 @path=#<Pathname:/home/gemmaro/src/sample-app>>'
            parsed = Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::Rails::Paths::Root])
            assert_instance_of Tcepsni::Vendor::Rails::Paths::Root, parsed
            assert_equal({ path: Pathname('/home/gemmaro/src/sample-app') }, parsed.attributes)
          end

          test 'omitted' do
            str = '#<Rails::Paths::Root:0x00007f97d8b264d0 ...>'
            assert_instance_of Tcepsni::Vendor::Rails::Paths::Root,
                               Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::Rails::Paths::Root])
          end
        end
      end
    end
  end
end

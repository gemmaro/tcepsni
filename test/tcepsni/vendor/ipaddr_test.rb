require 'test_helper'
require 'tcepsni/vendor/ipaddr'

module Tcepsni
  module Vendor
    class IPAddrTest < ::TestCase
      test 'IPAddr' do
        val = ::IPAddr.new('192.168.2.0/24')
        str = '#<IPAddr: IPv4:192.168.2.0/255.255.255.0>'
        assert_equal str, val.inspect
        assert_equal val, Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::IPAddr])
      end

      test 'IPAddr (IPv6)' do
        val = ::IPAddr.new('0000:0000:0000:0000:0000:0000:0000:0000/0000:0000:0000:0000:0000:0000:0000:0000')
        str = '#<IPAddr: IPv6:0000:0000:0000:0000:0000:0000:0000:0000/0000:0000:0000:0000:0000:0000:0000:0000>'
        assert_equal str, val.inspect
        assert_equal val, Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::IPAddr])
      end
    end
  end
end

require "test_helper"

module Tcepsni
  class ParserTest < ::Test::Unit::TestCase
    test 'integer' do
      val = 42
      str = '42'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'float' do
      val = 3.14
      str = '3.14'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'symbol' do
      val = :hello
      str = ':hello'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'bracket symbol' do
      val = :[]
      str = ':[]'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'nil' do
      val = nil
      str = 'nil'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'nilpotent' do
      assert_equal ::Tcepsni::Parser::FAIL, ::Tcepsni.parse('nilpotent')
    end

    test 'true' do
      val = true
      str = 'true'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'false' do
      val = false
      str = 'false'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'string' do
      val = 'hello world'
      str = '"hello world"'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'string with quotes' do
      val = "I'm saying \"hello world\""
      str = "\"I'm saying \\\"hello world\\\"\""
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'range' do
      val = 2..5
      str = '2..5'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    test 'time' do
      val = ::Time.new(2023, 10, 5)
      str = '2023-10-05 00:00:00 +0900'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end

    class ArrayTest < TestCase
      test 'array' do
        val = [1, nil, 'hey']
        str = '[1, nil, "hey"]'
        assert_equal str, val.inspect
        assert_equal val, ::Tcepsni.parse(str)
      end

      test 'empty' do
        val = []
        str = '[]'
        assert_equal str, val.inspect
        assert_equal val, ::Tcepsni.parse(str)
      end
    end

    class HashTest < ::TestCase
      test 'hash' do
        val = { abc: :def }
        str = '{:abc=>:def}'
        assert_equal str, val.inspect
        assert_equal val, ::Tcepsni.parse(str)
      end

      test 'hash with multiple keyvals' do
        val = { 123 => 'abc', {} => [::Class] }
        str = '{123=>"abc", {}=>[Class]}'
        assert_equal str, val.inspect
        parsed = ::Tcepsni.parse(str)
        assert_equal val[123], parsed[123]
        klass = parsed[{}][0]
        assert_equal :Class, klass.name
        assert_equal [], klass.namespaces
      end

      test 'hash with multiple keyvals part 2' do
        val = { enabled: true, index_name: 'index' }
        str = '{:enabled=>true, :index_name=>"index"}'
        assert_equal str, val.inspect
        assert_equal val, ::Tcepsni.parse(str)
      end
    end

    class ClassTest < TestCase
      Sample = ::Class.new

      test 'class' do
        str = 'Tcepsni::ParserTest::ClassTest::Sample'
        assert_equal str, Sample.inspect.encode(::Encoding::UTF_8)
        parsed = ::Tcepsni.parse(str)
        assert_instance_of ::Tcepsni::Class, parsed
        assert_equal :Sample, parsed.name
        assert_equal %i[Tcepsni ParserTest ClassTest], parsed.namespaces
      end
    end

    test 'object' do
      val = ::Object.new
      pat = /\A#<Object:0x[0-9a-f]{16}>\Z/
      str = val.inspect
      assert_match pat, str
      parsed = ::Tcepsni.parse(str)
      assert_instance_of ::Tcepsni::Object, parsed
      klass = parsed.klass
      assert_instance_of ::Tcepsni::Class, klass
      assert_equal :Object, klass.name
      assert_equal [], klass.namespaces
      assert_instance_of ::Integer, parsed.memory_reference
    end

    test 'objects' do
      val = [::Object.new, ::Object.new]
      pat = /\A\[#<Object:0x[0-9a-f]{16}>, #<Object:0x[0-9a-f]{16}>\]\Z/
      str = val.inspect
      assert_match pat, str
      parsed = ::Tcepsni.parse(str)
      assert_instance_of ::Array, parsed
      assert_equal 2, parsed.size
      assert_instance_of ::Tcepsni::Class, parsed[0].klass
      assert_instance_of ::Tcepsni::Class, parsed[1].klass
    end

    class ObjectWithAttributesTest < TestCase
      class Sample2
        def initialize
          @attr1 = :attr1
          @attr2 = :attr2
        end
      end

      test 'object with some attributes' do
        val = Sample2.new
        str = val.inspect
        pat = /\A#<Tcepsni::ParserTest::ObjectWithAttributesTest::Sample2:0x[0-9a-f]{16} @attr1=:attr1, @attr2=:attr2>\Z/
        assert_match pat, str
        parsed = ::Tcepsni.parse(str)
        assert_equal({ attr1: :attr1, attr2: :attr2 }, parsed.attributes)
        assert_instance_of ::Integer, parsed.memory_reference
        klass = parsed.klass
        assert_instance_of ::Tcepsni::Class, klass
        assert_equal :Sample2, klass.name
        assert_equal %i[Tcepsni ParserTest ObjectWithAttributesTest], klass.namespaces
      end

      test 'objects with some attributes' do
        val = [Sample2.new, Sample2.new]
        str = val.inspect
        pat = /\[#<Tcepsni::ParserTest::ObjectWithAttributesTest::Sample2:0x[0-9a-f]{16} @attr1=:attr1, @attr2=:attr2>, #<Tcepsni::ParserTest::ObjectWithAttributesTest::Sample2:0x[0-9a-f]{16} @attr1=:attr1, @attr2=:attr2>\]/
        assert_match pat, str
        parsed = ::Tcepsni.parse(str)
        assert_instance_of ::Array, parsed
        assert_equal 2, parsed.size
        assert_instance_of ::Tcepsni::Class, parsed[0].klass
        assert_instance_of ::Tcepsni::Class, parsed[1].klass
      end
    end

    class ObjectWithoutAttributesTest < TestCase
      Sample3 = ::Class.new

      test 'object without attributes' do
        val = Sample3.new
        str = val.inspect
        pat = /\A#<Tcepsni::ParserTest::ObjectWithoutAttributesTest::Sample3:0x[0-9a-f]{16}>\Z/
        assert_match pat, str
        parsed = ::Tcepsni.parse(str)
        assert_equal({}, parsed.attributes)
      end
    end

    test 'encoding' do
      val = ::Encoding::UTF_8
      str = '#<Encoding:UTF-8>'
      assert_equal str, val.inspect
      assert_equal val, ::Tcepsni.parse(str)
    end
  end
end

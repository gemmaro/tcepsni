# Tcepsni changelog

## Unreleased

## 0.4.0 - 2024-08-04

### Changed

* `parse_string` now parses leading double quote
* `parse_array` now parses leading bracket
* `parse_hash` now parses leading brace
* `parse_object` now consider vendors
* `parse_object`, `parse_integer`, `parse_expression`, and `parse_attributes` do not raise errors

### Removed

* `Tcepsni::Class#encoding?`

### Added

* Simple `Range` support and `parse_range`
* `parse_float`
* `parse_time`
* `parse_attribute`
* `parse_encoding`

## 0.3.1 - 2024-07-21

Update gemspec fields etc.
There should be no API changes.

## 0.3.0 - 2023-11-18

### Changed

- Introduced plugin system.
  For example, if the source contains `Pathname` objects, use the `vendors` option like `Tcepsni.parse(str, vendors: [::Tcepsni::Vendor::Pathname])`.

### Removed

- `Tcepsni::Class#string_scanner?`, `Tcepsni::Class#pathname?`, `Tcepsni::Class#ipaddr?`, `Tcepsni::Class#active_support_ordered_options?`, and `Tcepsni::Class#rails_paths_root?`.

## 0.2.0 - 2023-10-20

### Added

- `Time` support
- `Pathname` support
- `Encoding` support
- `IPAddr` support
- `ActiveSupport::OrderedOptions` support
- `Rails::Paths::Root` support
- `Float` support

### Fixed

- Parsing the empty array
- Parsing the object with some attributes
- Parsing the `nil`, `true`, and `false` literals
- Detecting `StringScanner`-like object

## 0.1.1 - 2023-10-02

### Changed

- Object's memory reference attribute has changed to integer from string

### Fixed

- String scanner parser now supports string with quotes

## 0.1.0 - 2023-10-01

- Initial release

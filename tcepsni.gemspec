require_relative 'lib/tcepsni/version'

Gem::Specification.new do |spec|
  spec.name = 'tcepsni'
  spec.version = Tcepsni::VERSION
  spec.authors = ['gemmaro']
  spec.email = ['gemmaro.dev@gmail.com']

  spec.summary = 'inspected Ruby objects parser'
  spec.description = 'Tcepsni gem is a parser library for inspected strings of Ruby objects.'
  spec.license = 'Apache-2.0'

  spec.homepage = 'https://codeberg.org/gemmaro/tcepsni'

  spec.required_ruby_version = '>= 3.2'

  spec.files = Dir['lib/**/*.rb'] + Dir['sig/**/*.rbs'] +
               ['CHANGELOG.md', 'tcepsni.gemspec', 'README.md', 'LICENSE.txt']
  spec.require_paths = ['lib']

  spec.metadata = {
    'rubygems_mfa_required' => 'true',
    'bug_tracker_uri' => 'https://codeberg.org/gemmaro/tcepsni/issues',
    'changelog_uri' => 'https://codeberg.org/gemmaro/tcepsni/src/branch/main/CHANGELOG.md',
    'documentation_uri' => 'https://www.rubydoc.info/gems/tcepsni',
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'wiki_uri' => 'https://codeberg.org/gemmaro/tcepsni/wiki'
  }
end

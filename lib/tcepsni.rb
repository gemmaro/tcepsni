require_relative 'tcepsni/version'
require_relative 'tcepsni/parser'

module Tcepsni
  Error = Class.new(StandardError)

  def self.parse(str, vendors: [])
    Parser.new(str, vendors:).parse
  end

  class Class
    attr_reader :name, :namespaces

    def initialize(name:, namespaces:)
      @name = name
      @namespaces = namespaces
    end
  end

  class Object
    attr_reader :klass, :attributes, :memory_reference

    def initialize(klass:, attributes:, memory_reference:)
      @klass = klass
      @attributes = attributes
      @memory_reference = memory_reference
    end
  end
end

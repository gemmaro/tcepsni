module Tcepsni
  module Vendor
    class StringScanner
      attr_reader :pos, :total, :scanned, :rest

      def initialize(pos:, total:, scanned:, rest:)
        @pos     = pos
        @total   = total
        @scanned = scanned
        @rest    = rest
      end

      def self.parse(parser)
        pos = parser.pos
        parser.skip('StringScanner ')   and
          pos = parser.parse_integer    and
          parser.skip('/')              and
          total = parser.parse_integer  and
          parser.skip(' ')              and
          scanned = parser.parse_string and
          parser.skip(' @ ')            and
          rest = parser.parse_string    and
          parser.skip('>')              and
          Tcepsni::Vendor::StringScanner.new(pos:, total:, scanned:, rest:) or
          begin
            parser.pos = pos
            nil
          end
      end

      def self.dependencies
        []
      end

      def self.object?
        true
      end
    end
  end
end

require 'ipaddr'

module Tcepsni
  module Vendor
    class IPAddr
      def self.parse(parser)
        parser.skip(/IPAddr: IPv[46]:(?<addr>[^>]+)>/) && ::IPAddr.new(parser[:addr])
      end

      def self.object?
        true
      end

      def self.dependencies
        []
      end
    end
  end
end

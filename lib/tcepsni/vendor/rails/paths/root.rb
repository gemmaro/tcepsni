require 'rails/paths'

module Tcepsni
  module Vendor
    module Rails
      module Paths
        class Root
          attr_reader :memory_reference, :attributes

          def initialize(memory_reference, attributes)
            @memory_reference = memory_reference
            @attributes = attributes
          end

          def self.parse(parser)
            parser.skip('Rails::Paths::Root') or return
            pos = parser.pos
            if (memory_reference = parser.parse_memory_reference)
              if parser.skip(' ...>')
                Tcepsni::Vendor::Rails::Paths::Root.new(memory_reference, {})
              elsif (attrs = parser.parse_attributes) && parser.skip('>')
                Tcepsni::Vendor::Rails::Paths::Root.new(memory_reference, attrs)
              else
                parser.pos = pos
                return
              end
            else
              parser.pos = pos
              nil
            end
          end

          def self.object?
            true
          end

          def self.dependencies
            [::Tcepsni::Vendor::Pathname]
          end
        end
      end
    end
  end
end

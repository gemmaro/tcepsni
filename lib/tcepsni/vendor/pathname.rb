require 'pathname'

module Tcepsni
  module Vendor
    class Pathname
      def self.parse(parser)
        parser.skip(/Pathname:(?<path>[^>]+)>/) && ::Pathname.new(parser[:path])
      end

      def self.object?
        true
      end

      def self.dependencies
        []
      end
    end
  end
end

require 'active_support/ordered_options'

module Tcepsni
  module Vendor
    module ActiveSupport
      class OrderedOptions
        def self.parse(parser)
          pos = parser.pos
          parser.skip('ActiveSupport::OrderedOptions ') and
            hash = parser.parse_hash                    and
            parser.skip('>') or
            begin
              parser.pos = pos
              return
            end
          options = ::ActiveSupport::OrderedOptions.new
          hash.each { |key, val| options[key] = val }
          options
        end

        def self.dependencies
          []
        end

        def self.object?
          true
        end
      end
    end
  end
end

require 'strscan'
require 'forwardable'

module Tcepsni
  class Parser
    FAIL = Data.define

    def initialize(source, vendors:)
      @scanner = ::StringScanner.new(source)
      @vendors = vendors.flat_map { |vendor| [*vendor.dependencies, vendor] }.uniq
    end

    # If it returned nil when failed, it collides with normal nil value.
    def parse_expression
      FAIL == (expr = parse_nil)   or  return expr
      FAIL == (expr = parse_false) or  return expr
      (expr = (parse_true          or
               parse_symbol        or
               parse_string        or
               parse_array         or
               parse_hash          or
               parse_encoding      or
               parse_object        or
               parse_range         or
               parse_float         or
               parse_time          or
               parse_integer       or
               parse_class))       and return expr
      FAIL
    end

    alias parse parse_expression

    def parse_nil
      @scanner.skip(/nil\b/) or return FAIL
      nil
    end

    def parse_true
      @scanner.skip(/true\b/) and true
    end

    def parse_false
      @scanner.skip(/false\b/) or return FAIL
      false
    end

    def parse_integer
      @scanner.scan(/\d+/)&.to_i
    end

    def parse_identifier
      @scanner.scan(/[A-Za-z_][A-Za-z0-9_]*/)&.intern
    end

    def parse_symbol
      pos = @scanner.pos
      @scanner.skip(':') and
        begin
          (id = parse_identifier) and return id
          @scanner.skip('[]')     and return :[]
          @scanner.pos = pos
          return
        end
    end

    def parse_string
      pos = @scanner.pos
      @scanner.skip('"') or return
      result = +''
      result <<
        if    @scanner.skip('"')   then return result
        elsif @scanner.skip('\\"') then '"'
        else                            @scanner.getch
        end until @scanner.eos?
      @scanner.pos = pos
      nil
    end

    def parse_array
      pos = @scanner.pos
      @scanner.skip('[') or return
      result = []
      until @scanner.eos?
        FAIL == (expr = parse_expression) and break
        result << expr
        @scanner.skip(', ') or break
      end
      if @scanner.skip(']')
        result
      else
        @scanner.pos = pos
        nil
      end
    end

    def parse_hash
      pos = @scanner.pos
      @scanner.skip('{') or return
      result = {}
      until @scanner.eos?
        FAIL == (key = parse_expression) and break
        @scanner.skip('=>') and
          FAIL != (value = parse_expression) or
          begin
            @scanner.pos = pos
            return
          end
        result[key] = value
        @scanner.skip(', ') or break
      end
      if @scanner.skip('}')
        result
      else
        @scanner.pos = pos
        return
      end
    end

    def parse_class
      pos = @scanner.pos
      namespaces = [(parse_capitalized_identifier or return)]
      namespaces <<
        begin
          parse_capitalized_identifier or
            begin
              @scanner.pos = pos
              return
            end
        end while @scanner.skip('::')
      *namespaces, name = namespaces
      Tcepsni::Class.new(name:, namespaces:)
    end

    def parse_capitalized_identifier
      @scanner.scan(/[A-Z][A-Za-z0-9_]*/)&.intern
    end

    def parse_encoding
      @scanner.skip(/#<Encoding:(?<name>[^>]+)>/) && Encoding.find(@scanner[:name])
    end

    def parse_object
      pos = @scanner.pos
      @scanner.skip('#<') or return
      @vendors.select { |vendor| vendor.object? }.each do |vendor|
        expr = vendor.parse(self) and return expr
      end

      klass = parse_class                         and
        memory_reference = parse_memory_reference and
        attributes = parse_attributes             and
        @scanner.skip('>')                        and
        Tcepsni::Object.new(klass:, memory_reference:, attributes:) or
        begin
          @scanner.pos = pos
          nil
        end
    end

    def parse_attribute
      pos = @scanner.pos
      @scanner.skip('@') and
        key = parse_identifier and
        @scanner.skip('=') and
        if FAIL == (value = parse_expression)
          @scanner.pos = pos
          return
        else
          return key, value
        end
    end

    def parse_attributes
      @scanner.match?('>') and return {}
      pos = @scanner.pos
      attributes = {}
      while @scanner.skip(' ')
        key, value = (parse_attribute or return)
        attributes[key] = value
        @scanner.skip(',') or
          if @scanner.match?('>')
            break
          else
            @scanner.pos = pos
            return
          end
      end
      attributes
    end

    def parse_memory_reference
      @scanner.skip(/:(?<ref>0x[0-9a-f]{16})/) or return
      Integer(@scanner[:ref])
    end

    def parse_range
      pos = @scanner.pos
      int = parse_integer and
        if @scanner.skip("..") && (int2 = parse_integer)
          int..int2
        else
          @scanner.pos = pos
          nil
        end
    end

    def parse_float
      pos = @scanner.pos
      int = parse_integer and
        if @scanner.skip('.') && (int2 = parse_integer)
          Float("#{int}.#{int2}")
        else
          @scanner.pos = pos
          nil
        end
    end

    def parse_time
      pos = @scanner.pos
      int = parse_integer and
        if int <= 9999 && @scanner.skip(/-(?<month>[01]\d)-(?<day>\d\d) (?<hour>[012]\d):(?<minute>[0-5]\d):(?<second>[0-5]\d) (?<zone>[+-]\d\d\d\d)/)
          Time.new(int, @scanner[:month], @scanner[:day],
                   @scanner[:hour], @scanner[:minute], @scanner[:second],
                   @scanner[:zone])
        else
          @scanner.pos = pos
          nil
        end
    end

    extend Forwardable
    def_delegators :@scanner, :skip, :[], :match?, :pos, :pos=
  end
end
